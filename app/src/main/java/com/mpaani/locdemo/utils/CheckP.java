package com.mpaani.locdemo.utils;

import android.content.Context;
import android.location.Criteria;
import android.location.LocationManager;

/**
 * Created by karan_000 on 11-03-2016.
 */
public class CheckP {

    public static boolean accessLocation(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        String bestProvider = locationManager.getBestProvider(criteria, false);
        if (bestProvider == null) {
            //No android.permission-group.LOCATION
            return false;
        }
        return true;
    }
}
