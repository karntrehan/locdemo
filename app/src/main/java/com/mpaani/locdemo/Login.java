package com.mpaani.locdemo;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.littlefluffytoys.littlefluffylocationlibrary.LocationLibrary;
import com.mpaani.locdemo.utils.CheckNetwork;
import com.mpaani.locdemo.utils.CheckP;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.tajchert.nammu.Nammu;
import pl.tajchert.nammu.PermissionCallback;

public class Login extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    @Bind(R.id.textView)
    TextView textView;
    @Bind(R.id.etUserName)
    EditText etUserName;
    @Bind(R.id.etPassword)
    EditText etPassword;
    @Bind(R.id.fabLogin)
    FloatingActionButton fabLogin;
    @Bind(R.id.rl_inner)
    RelativeLayout rlInner;

    @OnClick(R.id.fabLogin)
    public void submit(View view) {
        performLogin();
    }

    //dummy vals
    String expUsername = "mp";
    String expPassword = "mp";
    String userId = "1001";

    String username, password;
    Context mCon;
    CheckNetwork cn;
    boolean sendForward = false;
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        //Permissions
        Nammu.init(getApplicationContext());

        mCon = this;
        cn = new CheckNetwork(mCon);
        prefs = PreferenceManager.getDefaultSharedPreferences(mCon);

       /* if (prefs.getBoolean("LoggedIn", false)) {
            mCon.startActivity(new Intent(mCon, DisplayActivity.class));
            finish();
        }*/


    }

    //Permissions
    final PermissionCallback permissionLocationCallback = new PermissionCallback() {
        @Override
        public void permissionGranted() {
            boolean hasAccess = CheckP.accessLocation(mCon);
            Toast.makeText(mCon, "Access granted = " + hasAccess, Toast.LENGTH_SHORT).show();
            gotoHome();
        }

        @Override
        public void permissionRefused() {
            boolean hasAccess = CheckP.accessLocation(mCon);
            Toast.makeText(mCon, "Access granted = " + hasAccess, Toast.LENGTH_SHORT).show();
        }
    };

    private void performLogin() {
        username = etUserName.getText().toString().trim();
        password = etPassword.getText().toString().trim();

        if (username.equals("")) {
            etUserName.setError("Cannot be empty");
        } else if (password.equals("")) {
            etPassword.setError("Cannot be empty");
        } else {
            if (cn.isConnectingToInternet()) {
                if (username.equals(expUsername) && password.equals(expPassword)) {
                    Toast.makeText(mCon, "Login Success", Toast.LENGTH_LONG).show();

                    //Saving sharedprefs

                    prefs.edit().putBoolean("LoggedIn", true).apply();
                    prefs.edit().putString("UserId", userId).apply();

                    new MaterialDialog.Builder(mCon)
                            .title(":)")
                            .content(Html
                                    .fromHtml("Welcome <b>" + username + "</b>. Please make sure your GPS is active at all times. Also try to " +
                                            "keep the internet active as much as possible to make the app a better experience."))
                            .positiveText("Okay")
                            .negativeText("GPS")
                            .neutralText("Internet")
                            .callback(new MaterialDialog.ButtonCallback() {
                                @Override
                                public void onPositive(MaterialDialog dialog) {
                                    checkPermissions();
                                }

                                @Override
                                public void onNegative(MaterialDialog dialog) {
                                    sendForward = true;
                                    mCon.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }

                                @Override
                                public void onNeutral(MaterialDialog dialog) {
                                    sendForward = true;
                                    mCon.startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
                                }
                            })
                            .show();
                } else {
                    etPassword.setText("");
                    Toast.makeText(mCon, "Values incorrect", Toast.LENGTH_LONG).show();
                }

            } else {
                cn.showAlert();
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sendForward) {
            mCon.startActivity(new Intent(mCon, DisplayActivity.class));
            finish();
        }
    }

    private void checkPermissions()
    {
        if(Nammu.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
            boolean hasAccess = CheckP.accessLocation(this);
            Toast.makeText(this, "Access granted fine = " + hasAccess, Toast.LENGTH_SHORT).show();
            gotoHome();
        } else {
            if (Nammu.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                //User already refused to give us this permission or removed it
                //Now he/she can mark "never ask again" (sic!)
                Snackbar.make(rlInner, "Here we explain user why we need to know his/her location.",
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction("OK", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Nammu.askForPermission(Login.this, Manifest.permission.ACCESS_FINE_LOCATION, permissionLocationCallback);
                            }
                        }).show();
            } else {
                //First time asking for permission
                // or phone doesn't offer permission
                // or user marked "never ask again"
                Nammu.askForPermission(Login.this, Manifest.permission.ACCESS_FINE_LOCATION, permissionLocationCallback);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Nammu.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void gotoHome()
    {
        LocationLibrary.showDebugOutput(true);
        try {
            LocationLibrary.initialiseLibrary(getBaseContext(), 1 * 60 * 1000, 2 * 60 * 1000, "com.mpaani.locdemo");
        } catch (UnsupportedOperationException ex) {
            Log.d("TestApplication", "UnsupportedOperationException thrown - the device doesn't have any location providers");
        }
        mCon.startActivity(new Intent(mCon,DisplayActivity.class));
        finish();
    }

}
