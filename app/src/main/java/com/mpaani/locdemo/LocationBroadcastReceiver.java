package com.mpaani.locdemo;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.littlefluffytoys.littlefluffylocationlibrary.LocationInfo;
import com.littlefluffytoys.littlefluffylocationlibrary.LocationLibraryConstants;

import okhttp3.OkHttpClient;

public class LocationBroadcastReceiver extends BroadcastReceiver {

    Context mCon;

    String latitude, longitude, accuracy, provider, timestamp;

    private final OkHttpClient client = new OkHttpClient();

    SharedPreferences prefs;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("Location", "onReceive: received location update");

        mCon = context;

        prefs = PreferenceManager.getDefaultSharedPreferences(mCon);

        final LocationInfo locationInfo = (LocationInfo) intent.getSerializableExtra(LocationLibraryConstants.LOCATION_BROADCAST_EXTRA_LOCATIONINFO);
        // The broadcast has woken up your app, and so you could do anything now -
        timestamp = LocationInfo.formatTimeAndDay(locationInfo.lastLocationUpdateTimestamp, true);
        latitude = Float.toString(locationInfo.lastLat);
        longitude = Float.toString(locationInfo.lastLong);
        accuracy = Integer.toString(locationInfo.lastAccuracy) + "m";
        provider = locationInfo.lastProvider;

        if (prefs.getBoolean("LoggedIn", false) && !latitude.equals("0.0") && !longitude.equals("0.0")) {
            Intent notificationIntent = new Intent(context, DisplayActivity.class);
            PendingIntent contentIntent = PendingIntent.getActivity(context,
                    1221, notificationIntent,
                    PendingIntent.FLAG_CANCEL_CURRENT);
            NotificationManager nm = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            Resources res = context.getResources();
            Notification.Builder builder = new Notification.Builder(context);
            builder.setContentIntent(contentIntent)
                    .setSmallIcon(R.drawable.ic_location_on_white_48dp)
                    .setLargeIcon(BitmapFactory.decodeResource(res, R.mipmap.ic_launcher))
                    .setTicker("Location update!")
                    .setWhen(System.currentTimeMillis())
                    .setAutoCancel(true)
                    .setContentTitle("Location updated")
                    .setContentText("Location Update " + timestamp+ ", Latitude " + latitude + ", Longitude " + longitude);
            Notification n = builder.build();
            nm.notify(1221, n);
        }

        Toast.makeText(context,"Location",Toast.LENGTH_LONG).show();
    }
}
